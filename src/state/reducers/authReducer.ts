import { ActionType } from '../action-types';
import { Action } from '../actions';

interface AppState {
  isSignedIn: boolean | null;
}

const initialState: AppState = {
  isSignedIn: null,
};

const reducer = (state: AppState = initialState, action: Action): AppState => {
  switch (action.type) {
    case ActionType.SIGN_IN:
      return { ...state, isSignedIn: true };
    case ActionType.SIGN_OUT:
      return { ...state, isSignedIn: false };

    default:
      return state;
  }
};

export default reducer;
