import React from 'react';
import { Link } from 'react-router-dom';

const RegisterPage = () => {
  return (
    <div>
      <h1>Register page</h1>
      <Link to="/">Home</Link>
    </div>
  );
};

export default RegisterPage;
