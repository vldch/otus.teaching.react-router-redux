import React from 'react'
import { Link } from 'react-router-dom'

const LoginPage = () => {
  return (
    <div>
      <h1>Login page</h1>
      <Link to='/'>Home</Link>
    </div>
  )
}

export default LoginPage